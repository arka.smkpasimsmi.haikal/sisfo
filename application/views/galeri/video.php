<div class="team">
<!-- Popular Courses -->

<div class="courses">
		<div class="section_background parallax-window" data-parallax="scroll" data-image-src="<?= base_url('assets/images/courses_background.jpg'); ?>" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<h2 class="section_title">Galeri Video Kegiatan Sekolah</h2>
						<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>
					</div>
				</div>
			</div>
			<div class="row courses_row">
				
			<?php  foreach ($video as $key) : ?>
				<?php $url = $key->item;  ?>
				<?php $value = explode("v=", $url); ?>
				<?php $videoId = $value[1]; ?>
			
				<!-- Course -->
				<div class="col-lg-4 course_col mt-4">
					<div class="course">
						<div class="course_image"><iframe width="100%" height="200px" src="https://www.youtube.com/embed/<?= $videoId; ?>"></iframe></div>
						<div class="course_body">
							<h3 class="course_title"><a><?= $key->judul; ?></a></h3>
							<div class="course_text">
								<p><?= $key->deskripsi; ?></p>
							</div>
						</div>
						<div class="course_footer">
						</div>
					</div>
				</div>

			<?php endforeach; ?>

				

			</div>
		</div>
	</div>
</div> 