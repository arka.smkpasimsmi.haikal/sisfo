<div class="team">
	<div class="courses">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<h2 class="section_title">Prestasi</h2>
							<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>
					</div>
				</div>
			</div>
			<div class="row events_row">

				<!-- Event -->
				<?php foreach ($prestasi as $data) : ?>
					<div class="col-lg-4 event_col">
						<div class="event event_left">
							<a href="<?= base_url('assets/images/prestasi_images/'.$data->foto); ?>" class="single-popup-photo">
                                <div class="event_image"><img style="width:100%;max-height: 252px;object-fit: cover;object-position: center;cursor:pointer;" src="<?= base_url('assets/images/prestasi_images/'.$data->foto); ?>" ></div>
                            </a>
							<div class="event_body d-flex flex-row align-items-start justify-content-start">
								<div class="event_date">
									<div class="d-flex flex-column align-items-center justify-content-center trans_200">
										<div class="event_day trans_200"><?= date('d', strtotime($data->created_dt)); ?></div>
										<div class="event_month trans_200"><?= date('M', strtotime($data->created_dt)); ?></div>
									</div>
								</div>
								<div class="event_content">
									<div class="event_title"><a href="#"><?= $data->nama_prestasi; ?></a></div>
									<div class="event_info_container">
										<div class="event_info"><i class="fa fa-clock-o" aria-hidden="true"></i><span><?= date('d-m-Y', strtotime($data->created_dt)); ?></span></div>
										<div class="event_text">
											<p><?= $data->deskripsi; ?></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>

			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	        	<img id="popup-img" src="" alt="" class="img-fluid">
	      </div>
	    </div>
	  </div>
	</div>

<script>
	function popupImagePrestasi(){
		$('.popup').click(function(){
			const src = $(this).attr('src');
			$('.modal').modal('show');
			$('#popup-img').attr('src', src);
		});
	}
</script>