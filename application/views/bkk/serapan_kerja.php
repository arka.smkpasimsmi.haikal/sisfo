<div class="team">
	<div class="contact">

		<!-- Contact Info -->
		<div class="contact_info_container">
			<div class="container">
            <div class="mt-5">
            	<div class="col">
					<div class="section_title_container text-center pt-5">
						<h2 class="section_title">Serapan Kerja</h2>
							<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>
					</div>
				</div>
            </div>
				<div class="row pt-5">
					<?php $no=1; ?>
						<?php foreach ($serapan_kerja as $data): ?>
							<?php $no++; ?>
								<?php if($no % 2 == 0) : ?>
								<!-- Info Serapan Kerja Kiri -->
									<div class="container mt-4 pt-4">
									<div class="row">
										<div class="col-md-6">
											<div class="mt-4">
													<h4 class="course_title"><a href="<?= base_url('Bkk/detailSerapan/'.$data->id) ?>"><?= $data->judul ?></a></h4>
													<!-- <div class="course_teacher">SMKN 1</div> -->
													<div class="course_text">
														<p><?= $data->deskripsi; ?></p>
													</div>
											</div>
										</div>
								<!-- Serapan Kerja Kiri -->
										<div class="col-md-6">
											<a href="<?= base_url('assets/images/bkk_images/'.$data->item); ?>" class="single-popup-photo">
												<img style="width:100%;max-height: 340px;object-fit: cover;object-position: center;cursor:pointer;border-radius:5px;" src="<?= base_url('assets/images/bkk_images/'.$data->item) ?>" alt="">
											</a>
										</div>
									</div>
									</div>
								<?php else : ?>
								<!-- Serapan Kerja Kanan -->
								<div class="container mt-5 mt-4 pt-4">
									<div class="row">
										<div class="col-md-6">
											<a href="<?= base_url('assets/images/bkk_images/'.$data->item); ?>" class="single-popup-photo">
												<img style="width:100%;max-height: 340px;object-fit: cover;object-position: center;cursor:pointer;border-radius:5px;" src="<?= base_url('assets/images/bkk_images/'.$data->item) ?>" alt="">
											</a>
										</div>
								<!-- Info Serapan Kerja Kanan -->
										<div class="col-md-6">
											<div class="mt-4">
												<h4 class="course_title"><a href="<?= base_url('Bkk/detailSerapan/'.$data->id) ?>"><?= $data->judul ?></a></h4>
												<!-- <div class="course_teacher">SMKN 1</div> -->
												<div class="course_text">
													<p><?= $data->deskripsi; ?></p>
												</div>
											</div>
										</div>
									</div>
									</div>	
								<?php endif; ?>
						<?php endforeach; ?>			
					
				</div>
			</div>
		</div>


	</div>
</div>