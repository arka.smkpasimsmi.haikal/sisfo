<?php foreach($guru as $d) : ?>

    <!-- roleguru -->
    <?php 
    $id     = $this->session->userdata('id');
    $data   = $this->db->get_where('auth', ['guru_id' => $id])->row_array();;
    if ($this->session->userdata('role') == 'guru'): ?>
        <?php if ($d->id == $id): ?>
<form method="POST" action="<?= base_url('Admin_guru/deleteGuru/'.$id) ?>" id="form-delete" ></form>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-file-text bg-blue"></i>
                <div class="d-inline">
                    <h5>Profil Guru</h5>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 col-md-5">
        <div class="card">
            <div class="card-body">
                <div class="text-center"> 
                    <a href="<?= base_url('assets/images/guru_images/'.$d->foto) ?>" class="single-popup-photo">
                        <img src="<?= base_url('assets/images/guru_images/'.$d->foto) ?>" class="img-thumbnail crop-foto" width="150" height="500">
                    </a>
                    <h4 class="card-title mt-10"><?= $d->nama_guru ?></h4>
                    <p class="card-subtitle"><?= $d->mapel ?></p>
                </div>
            </div>
            <hr class="mb-0"> 
            <div class="card-body"> 
                <small class="text-muted d-block">Email address </small>
                <h6><?= $data['username'] ?></h6> 
                <small class="text-muted d-block pt-10">Tanggal Masuk</small>
                <h6><?= $d->tgl_masuk ?></h6> 
                <small class="text-muted d-block pt-30">Social Profile</small>
                <br>
                <?php if ($d->facebook == " ") {
                            $facebook ="#";
                        }else{
                            $facebook ="$d->facebook";
                        } 
                    ?>
                <a href="<?= $facebook ?>"><button class="btn btn-icon btn-facebook" target="_blank"><i class="fab fa-facebook-f"></i></button></a>
                <a href="<?= 'https://www.instagram.com/'.$d->instagram ?>" target="_blank"><button class="btn btn-icon btn-instagram"><i class="fab fa-instagram"></i></button></a>
                <!-- <button class="btn btn-icon btn-dark"><i class="fab fa-google-plus-g"></i></button> -->
                <div class="text-center mt-2">
                    <button class="btn btn-danger" type="submit" id="btn-delete-submit">Hapus Akun</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-7">
        <div class="card">
            <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" id="pills-setting-tab" data-toggle="pill" href="#previous-month" role="tab" aria-controls="pills-setting" aria-selected="false">Profil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#akun" role="tab" aria-controls="pills-setting" aria-selected="false">Settings</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade active show" id="previous-month" role="tabpanel" aria-labelledby="pills-setting-tab">
                    <div class="card-body">
                        <form class="form-horizontal" method="POST" action="<?= base_url('Admin_guru/postEdit/'.$id) ?>" enctype="multipart/form-data" id="form-edit">
                            <input type="hidden" name="foto_lama" value="<?= $d->foto ?>">
                            <div class="form-group">
                                <a href="<?= base_url('assets/images/guru_images/'.$d->foto) ?>" class="single-popup-photo">
                                    <img src="<?= base_url('assets/images/guru_images/'.$d->foto) ?>" class="rounded-circle" width="150" height="150">
                                </a>
                            </div>
                            <div class="form-group">
                                <label>Foto</label>
                                <input type="file" class="form-control file-upload-info" placeholder="Upload Image" name="foto" >
                                <small>(Biarkan kosong jika tidak ingin diganti)</small>
                            </div>
                            <div class="form-group">
                                <label for="example-name">Nama Lengkap</label>
                                <input type="text" placeholder="Nama Lengkap" class="form-control" name="nama_lengkap" id="example-name" value="<?= $d->nama_guru ?>">

                            </div>
                            <div class="form-group">
                                <label for="example-email">Mata Pelajaran</label>
                                <input type="text" placeholder="Email" class="form-control" name="mapel" id="example-email" value="<?= $d->mapel ?>">
                            </div>
                            <div class="form-group">
                                <label for="example-phone">Tanggal Masuk</label>
                                <input id="dropper-default" type="text" placeholder="Tanggal Masuk" id="example-phone" name="tgl_masuk" class="form-control" value="<?= $d->tgl_masuk ?>" readonly>
                            </div>
                            <h4 class="mt-30 border-bottom">Sosial Media</h4>
                            <div class="form-group">
                                <label for="example-password">Facebook</label>
                                <input type="text" value="<?= $d->facebook ?>" class="form-control" name="facebook" id="example-password" placeholder="https://web.facebook.com/nickname/">
                            </div>
                            <div class="form-group">
                                <label for="example-password">Instagram</label>
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <label class="input-group-text">@</label>
                                    </span>
                                    <input type="text" value="<?= $d->instagram ?>" class="form-control" name="instagram" placeholder="nickname">
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label for="example-password">Google +</label>
                                <input type="email" value="<?= $d->gmail ?>" class="form-control" name="gmail" id="example-password" placeholder="Google +">
                            </div> -->
                            <button class="btn btn-success" type="submit" id="btn-edit-submit">Update Profil</button>
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade " id="akun" role="tabpanel" aria-labelledby="pills-setting-tab">
                    <div class="card-body">
                            <div class="card">
                                <div class="card-header">
                                    <h3>Ganti Email</h3>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option" style="width: 90px;">
                                            <li><i class="ik minimize-card ik-plus"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body feeds-widget" style="display: none;">
                                    <form class="form-horizontal" method="POST" action="<?= base_url('Admin_guru/gantiEmail/'.$id) ?>">
                                    <div class="feed-item">
                                        <div class="form-group">
                                            <label for="example-password">Email Lama</label>
                                            <input type="email" value="<?= $data['username'] ?>" class="form-control" name="email_lama" id="example-password" placeholder="Email Lama" readonly>
                                        </div>
                                    </div>
                                    <div class="feed-item">
                                        <div class="form-group">
                                            <label for="example-password">Email Baru</label>
                                            <input type="email" class="form-control" name="email_baru" id="example-password" placeholder="Email Baru">
                                            <small class="text-danger"><i><?= form_error('email_baru') ?></i></small>
                                        </div>
                                    </div>
                                    <div class="feed-item">
                                        <button class="btn btn-success" type="submit">Update Email</button>
                                    </div>
                                </form>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h3>Ganti Password</h3>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option" style="width: 90px;">
                                            <li><i class="ik minimize-card ik-plus"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body feeds-widget" style="display: none;">
                                    <form class="form-horizontal" method="POST" action="<?= base_url('Admin_guru/gantiPassword/'.$id) ?>" >
                                    <div class="feed-item">
                                        <div class="form-group">
                                            <label>Password Lama</label>
                                            <input type="password" class="form-control" name="password_lama" placeholder="Password Lama">
                                            <small class="text-danger"><i><?= form_error('password_lama') ?></i></small>
                                        </div>
                                    </div>
                                    <div class="feed-item">
                                        <div class="form-group">
                                            <label>Password Baru</label>
                                            <input type="password" class="form-control" name="password_baru" placeholder="Password Baru">
                                            <small class="text-danger"><i><?= form_error('password_baru') ?></i></small>
                                        </div>
                                    </div>
                                    <div class="feed-item">
                                        <button class="btn btn-success" type="submit">Update Password</button>
                                    </div>
                                </form>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif ?>
<?php elseif($this->session->userdata('role') == 'admin'): ?>
    <!-- roleadmin -->
    <form method="POST" action="<?= base_url('Admin_guru/deleteGuru/'.$d->id) ?>" id="form-delete" ></form>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-file-text bg-blue"></i>
                <div class="d-inline">
                    <h5>Profil Guru</h5>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="../index.html"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">Pages</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Profile</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 col-md-5">
        <div class="card">
            <div class="card-body">
                <div class="text-center"> 
                    <a href="<?= base_url('assets/images/guru_images/'.$d->foto) ?>" class="single-popup-photo">
                        <img src="<?= base_url('assets/images/guru_images/'.$d->foto) ?>" class="img-thumbnail crop-foto" width="150" height="500">
                    </a>
                    <h4 class="card-title mt-10"><?= $d->nama_guru ?></h4>
                    <p class="card-subtitle"><?= $d->mapel ?></p>
                </div>
            </div>
            <hr class="mb-0"> 
            <div class="card-body"> 
                <small class="text-muted d-block">Email address </small>
                <h6><?= $d->email ?></h6> 
                <small class="text-muted d-block pt-10">Tanggal Masuk</small>
                <h6><?= $d->tgl_masuk ?></h6> 
                <small class="text-muted d-block pt-30">Social Profile</small>
                <br>
                <?php if (!$d->facebook) {
                            $facebook ="#";
                        }else{
                            $facebook ="$d->facebook";
                        } 
                    ?>
                <a href="<?= $facebook ?>" ><button class="btn btn-icon btn-facebook"><i class="fab fa-facebook-f"></i></button></a>
                <a href="<?= 'https://www.instagram.com/'.$d->instagram ?>" target="_blank"><button class="btn btn-icon btn-instagram"><i class="fab fa-instagram"></i></button></a>
                <!-- <button class="btn btn-icon btn-dark"><i class="fab fa-google-plus-g"></i></button> -->
                <div class="text-center mt-2">
                    <button class="btn btn-danger" type="submit" id="btn-delete-submit">Hapus Akun</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-7">
        <div class="card">
            <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" id="pills-setting-tab" data-toggle="pill" href="#previous-month" role="tab" aria-controls="pills-setting" aria-selected="false">Profil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#akun" role="tab" aria-controls="pills-setting" aria-selected="false">Settings</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade active show" id="previous-month" role="tabpanel" aria-labelledby="pills-setting-tab">
                    <div class="card-body">
                        <form class="form-horizontal" method="POST" action="<?= base_url('Admin_guru/postEdit/'.$d->id) ?>" enctype="multipart/form-data" id="form-edit">
                            <input type="hidden" name="foto_lama" value="<?= $d->foto ?>">
                            <div class="form-group">
                                <a href="<?= base_url('assets/images/guru_images/'.$d->foto) ?>" class="single-popup-photo">
                                    <img src="<?= base_url('assets/images/guru_images/'.$d->foto) ?>" class="rounded-circle" width="150" height="150">
                                </a>
                            </div>
                            <div class="form-group">
                                <label>Foto</label>
                                <input type="file" class="form-control file-upload-info" placeholder="Upload Image" name="foto" >
                                <small>(Biarkan kosong jika tidak ingin diganti)</small>
                            </div>
                            <div class="form-group">
                                <label for="example-name">Nama Lengkap</label>
                                <input type="text" placeholder="Nama Lengkap" class="form-control" name="nama_lengkap" id="example-name" value="<?= $d->nama_guru ?>">

                            </div>
                            <div class="form-group">
                                <label for="example-email">Mata Pelajaran</label>
                                <input type="text" placeholder="Email" class="form-control" name="mapel" id="example-email" value="<?= $d->mapel ?>">
                            </div>
                            <div class="form-group">
                                <label for="example-phone">Tanggal Masuk</label>
                                <input id="dropper-default" type="text" placeholder="Tanggal Masuk" id="example-phone" name="tgl_masuk" class="form-control" value="<?= $d->tgl_masuk ?>" readonly>
                            </div>
                            <h4 class="mt-30 border-bottom">Sosial Media</h4>
                            <div class="form-group">
                                <label for="example-password">Facebook</label>
                                <input type="text" value="<?= $d->facebook ?>" class="form-control" name="facebook" id="example-password" placeholder="https://web.facebook.com/nickname/">
                            </div>
                            <div class="form-group">
                                <label for="example-password">Instagram</label>
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <label class="input-group-text">@</label>
                                    </span>
                                    <input type="text" value="<?= $d->instagram ?>" class="form-control" name="instagram" placeholder="nickname">
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label for="example-password">Google +</label>
                                <input type="email" value="<?= $d->gmail ?>" class="form-control" name="gmail" id="example-password" placeholder="Google +">
                            </div> -->
                            <button class="btn btn-success" type="submit" id="btn-edit-submit">Update Profil</button>
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade " id="akun" role="tabpanel" aria-labelledby="pills-setting-tab">
                    <div class="card-body">
                            <div class="card">
                                <div class="card-header">
                                    <h3>Ganti Email</h3>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option" style="width: 90px;">
                                            <li><i class="ik minimize-card ik-plus"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body feeds-widget" style="display: none;">
                                    <form class="form-horizontal" method="POST" action="<?= base_url('Admin_guru/gantiEmail/'.$d->id) ?>">
                                    <div class="feed-item">
                                        <div class="form-group">
                                            <label for="example-password">Email Lama</label>
                                            <input type="email" value="<?= $d->email ?>" class="form-control" name="email_lama" id="example-password" placeholder="Email Lama" readonly>
                                        </div>
                                    </div>
                                    <div class="feed-item">
                                        <div class="form-group">
                                            <label for="example-password">Email Baru</label>
                                            <input type="email" class="form-control" name="email_baru" id="example-password" placeholder="Email Baru">
                                            <small class="text-danger"><i><?= form_error('email_baru') ?></i></small>
                                        </div>
                                    </div>
                                    <div class="feed-item">
                                        <button class="btn btn-success" type="submit">Update Email</button>
                                    </div>
                                </form>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h3>Ganti Password</h3>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option" style="width: 90px;">
                                            <li><i class="ik minimize-card ik-plus"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body feeds-widget" style="display: none;">
                                    <form class="form-horizontal" method="POST" action="<?= base_url('Admin_guru/gantiPassword/'.$d->id) ?>" >
                                    <div class="feed-item">
                                        <div class="form-group">
                                            <label>Password Lama</label>
                                            <input type="password" class="form-control" name="password_lama" placeholder="Password Lama">
                                            <small class="text-danger"><i><?= form_error('password_lama') ?></i></small>
                                        </div>
                                    </div>
                                    <div class="feed-item">
                                        <div class="form-group">
                                            <label>Password Baru</label>
                                            <input type="password" class="form-control" name="password_baru" placeholder="Password Baru">
                                            <small class="text-danger"><i><?= form_error('password_baru') ?></i></small>
                                        </div>
                                    </div>
                                    <div class="feed-item">
                                        <button class="btn btn-success" type="submit">Update Password</button>
                                    </div>
                                </form>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php endif ?>
<?php endforeach; ?>