<?php foreach ($serapan as $d): ?>
	<?php if ($d->kategori == 'serapan'): ?>
		
<form method="POST" action="<?= base_url('Admin_bkk/deleteBkk/'.$d->id.'/serapan') ?>" id="form-delete" ></form>
<form method="POST" action="<?= base_url('Admin_bkk/postEditSerapan/'.$d->id) ?>" id="form-edit" enctype="multipart/form-data">
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <a href="<?= base_url('Admin_bkk/listSerapan') ?>"><i class="ik ik-arrow-left" style="background-color: #14bdee;"></i></a>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active show" id="pills-detail-tab" data-toggle="pill" href="#current-month" role="tab" aria-controls="pills-detail" aria-selected="true">Detail</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab" aria-controls="pills-profile" aria-selected="false">Edit</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade active show" id="current-month" role="tabpanel" aria-labelledby="pills-detail-tab">
            <div class="card-body">
				<div class="row feature_row">
					<!-- Feature Content -->
					<div class="col-lg-6 feature_col">
						<div class="feature_content">
							<div class="accordions">
								<div class="elements_accordions">
									<div class="accordion_container">
										<h3 class="section_title"><?= $d->judul ?></h3>
										<div class="border-bottom"></div>
										<div class="section_subtitle mt-2">
											<p><?= $d->deskripsi ?></p>
										</div>
										<h5 class="font-weight-bold">Kontak </h5>
											<strong>Kota : </strong><?= $d->kota ?><br>
								          	<strong>Alamat : </strong><?= $d->alamat ?><br>
								          	<strong>No.Telepon : </strong><?= $d->no_telp ?><br>
								          	<strong>Email : </strong><?= $d->email_perusahaan ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 feature_col">
						<div class="feature_video d-flex flex-column align-items-center justify-content-center">
							<img height="350" width="350" class="rounded-circle" src="<?= base_url('assets/images/bkk_images/'.$d->item) ?>" alt="FOTO">
						</div>
					</div>
				</div>
			</div>
        </div>
        <div class="tab-pane fade" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
            <div class="card-body">
				<div class="row feature_row">
					<!-- Feature Content -->
					<div class="col-lg-6 feature_col">
						<div class="feature_content">
							<div class="accordions">
								<div class="elements_accordions">
									<div class="accordion_container">
										<h3><input name="judul" type="text" value="<?= $d->judul ?>"></h3>
										<div class="border-bottom"></div>
										<div class=" mt-2">
											<textarea name="deskripsi" class="form-control" rows="10"><?= $d->deskripsi ?></textarea>
											<div class="form-group mt-2">
							                    <label>Kota Perusahaan</label>
							                    <input type="text" class="form-control" placeholder="Judul Foto" name="kota" value="<?= $d->kota ?>">
							                </div>
							                <div class="form-group mt-2">
							                    <label>Alamat Perusahaan</label>
							                    <input type="text" class="form-control" placeholder="Judul Foto" name="alamat" value="<?= $d->alamat ?>">
							                </div>
							                <div class="form-group mt-2">
							                    <label>No Telepon</label>
							                    <input type="text" class="form-control" placeholder="Judul Foto" name="no_telp" value="<?= $d->no_telp ?>">
							                </div>
							                <div class="form-group mt-2">
							                    <label>Email Perusahaan</label>
							                    <input type="text" class="form-control" placeholder="Judul Foto" name="email_perusahaan" value="<?= $d->email_perusahaan ?>">
							                </div>
											<button class="btn btn-warning mt-2" type="submit" id="btn-edit-submit">Simpan Perubahan</button>
											<button class="btn btn-danger mt-2" type="submit" id="btn-delete-submit">Hapus Data Serapan</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 feature_col">
						<div class="feature_video d-flex flex-column align-items-center justify-content-center">
							<img height="350" width="350" class="rounded-circle" src="<?= base_url('assets/images/bkk_images/'.$d->item) ?>" alt="FOTO">
							<div class="form-group mt-2">
			                    <label>Foto</label>
			                    <div class="input-group">
			                        <input type="file" class="form-control file-upload-info" placeholder="Upload Image" name="foto" ><small>(Biarkan kosong jika tidak ingin diubah)</small>
			                    </div>
			                    <input type="hidden" name="foto_lama" value="<?= $d->item ?>">
                			</div>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
</form>
	<?php endif ?>
<?php endforeach ?>