<tbody>
  <?php $i = 1; foreach ($pesan as $d): 
    if ($d->tipe_pesan=="penerima" && $d->sampah =='false'): ?>
    <?php $isi = explode(' ', $d->deskripsi); ?>
    <tr class="<?= $d->status == 'belum_dibaca' ? 'tema-abu' : ' ' ?>">
      <td class="text-center">
        <i class="<?= $d->status == 'belum_dibaca' ? 'ik ik-bell text-danger' : ' ' ?>"></i>
      </td>
      <?php if ($d->favorit=="true"): ?>
        <td class="mailbox-star"><a href="<?= base_url('Feedback/pesanFavorit/'.$d->id.'/list/unfavorit') ?>"><i class="fas fa-star text-warning"></i></a></td>
        <?php elseif($d->favorit=="false"): ?>
         <td class="mailbox-star"><a href="<?= base_url('Feedback/pesanFavorit/'.$d->id.'/list/favorit') ?>"><i class="fas fa-star text-secondary"></i></a></td> 
      <?php endif ?>
      <td class="mailbox-name"><a href="<?= base_url('Feedback/klikPesan/'.$d->id) ?>" class="text-dark">Dari :<span class="text-primary"><?= $d->nama ?></span></a></td>
      <td class="mailbox-subject"><b><?= $d->subject ?></b> - <?= $isi[0].'...' ?> 
      </td>
      <td><?= date('d-m-Y, H:i', strtotime($d->created_dt))  ?></td>
      <td>
        <div class="list-actions">
            <a href="<?= base_url('Feedback/buangPesan/'.$d->id.'/list') ?>"><i class="ik ik-trash-2"></i></a>
        </div>
      </td>
  </tr>
  <?php
  endif;
   endforeach; ?>
</tbody>
</form>
