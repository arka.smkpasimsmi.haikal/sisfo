<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller {
	
	public function listPesan()
	{
		$title['title'] = 'List Pesan';
		$data = [
			'pesan'	=> $this->m_pesan->getPesan()
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('templates/server_partial/sidemenu_mailbox');
		$this->load->view('templates/server_partial/tabel_mailbox',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');

	}

	public function terkirimPesan()
	{
		$title['title'] = 'Pesan Terkirim';
		$data = [
			'pesan'	=> $this->crud->get('tb_m_pesan')
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('templates/server_partial/sidemenu_mailbox');
		$this->load->view('templates/server_partial/tabel_mailbox',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');

	}

	public function pentingPesan()
	{
		$title['title'] = 'Pesan Penting';
		$data = [
			'pesan'	=> $this->crud->get('tb_m_pesan')
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('templates/server_partial/sidemenu_mailbox');
		$this->load->view('templates/server_partial/tabel_mailbox',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');

	}

	public function sampahPesan()
	{
		$title['title'] = 'Pesan Sampah';
		$data = [
			'pesan'	=> $this->crud->get('tb_m_pesan')
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('templates/server_partial/sidemenu_mailbox');
		$this->load->view('templates/server_partial/tabel_mailbox',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');

	}

	public function klikPesan($ids)
	{
		$id 	= ['id' => $ids];
		$data 	= ['status' => 'dibaca'];

		$this->crud->edit($id,$data,'tb_m_pesan');
		$this->bacaPesan($ids);
	}

	public function bacaPesan($id)
	{
		$id 			= ['id' => $id];
		$nama 			= $this->db->get_where('tb_m_pesan',$id)->row_array();
		$title['title'] = 'Feedback | '.$nama['email'];
		$data			= [
							'pesan'	=> $this->crud->getById('tb_m_pesan',$id)
							];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('templates/server_partial/sidemenu_mailbox');
		$this->load->view('server/feedback/baca_pesan',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function tulisPesan()
	{
		$title['title'] = 'Tulis Pesan';

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('templates/server_partial/sidemenu_mailbox');
		$this->load->view('server/feedback/tulis_pesan');
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function insertSentEmail()
	{
		$email 		= $this->input->post('email');
		$subject	= $this->input->post('subject');
		$isi 		= $this->input->post('isi');

		$data =[
			'nama'			=> 'ADMIN',
			'email'			=> $email,
			'subject'		=> $subject,
			'deskripsi' 	=> $isi,
			'status'		=> 'dibaca',
			'tipe_pesan'	=> 'pengirim',
			'favorit'		=> 'false',
			'sampah'		=> 'false',
			'status'		=> 'belum_dibaca',
			'created_by'	=> 'ADMIN'
		];

		$this->crud->insert($data,'tb_m_pesan');
		$this->kirimEmail($email, $subject, $isi);	
	}

	public function kirimEmail($email, $subject, $isi)
	{
		$config = [
        'mailtype'  => 'html',
        'charset'   => 'utf-8',
        'protocol'  => 'smtp',
        'smtp_host' => 'smtp.gmail.com',
        'smtp_user' => 'sekolahsisfo@gmail.com',  // Email gmail
        'smtp_pass'   => 'sekolahsisfo123',  // Password gmail
        'smtp_crypto' => 'ssl',
        'smtp_port'   => 465,
        'crlf'    => "\r\n",
        'newline' => "\r\n"
    	];

        $this->load->library('email', $config);
    	$this->email->from('sekolahsisfo@gmail.com', 'Sisfo');
    	$this->email->to($email);
    	$this->email->subject($subject);
    	$this->email->message($isi);

    	if ($this->email->send()) {
			$this->session->set_flashdata('success','Berhasil mengirim email!');
			Redirect('Feedback/listPesan');
    	}else{
			$this->session->set_flashdata('fail','Gagal mengirim email!');
			Redirect('Feedback/listPesan');
    	}

	}

	public function deletePesan()
	{
		$id = $this->input->post('id');
		$this->crud->delete($id,'tb_m_pesan');
		$this->session->set_flashdata('success','Sukses hapus data!');
		Redirect('Feedback/sampahPesan');
	}

	public function deletePesanDetail($id)
	{
		$id 	= ['id' => $id];
		$this->crud->delete($id,'tb_m_pesan');
		$this->session->set_flashdata('success','Sukses hapus data!');
		Redirect('Feedback/sampahPesan');
	}

	public function restorePesan($id)
	{
		$id 	= ['id' => $id];
		
		$data	= [
			'sampah'	=> 'false',
		];

		$this->crud->edit($id,$data,'tb_m_pesan');
		$this->session->set_flashdata('success', 'Sukses restore pesan!');
		Redirect('Feedback/sampahPesan');
		
	}

	public function buangPesan($ids,$menu)
	{
		$id 	= ['id' => $ids];
			
			$data	= [
				'sampah'	=> 'true',
				'favorit'	=> 'false'
			];

			$this->crud->edit($id,$data,'tb_m_pesan');
			$this->session->set_flashdata('success', 'Sukses buang pesan!');
			if ($menu == 'list') {
				Redirect('Feedback/listPesan');
			}elseif ($menu == 'kirim') {
				Redirect('Feedback/terkirimPesan');
			}elseif ($menu == 'penting') {
				Redirect('Feedback/pentingPesan');
			}elseif ($menu == 'detail') {
				Redirect('Feedback/bacaPesan/'.$ids);
			}
		
	}

	public function pesanFavorit($ids,$menu,$tipe)
	{
		if ($tipe == "favorit") {
			$id 	= ['id' => $ids];
			$data = [
				'favorit'	=> 'true'
			];
			$this->crud->edit($id,$data,'tb_m_pesan');
			$this->session->set_flashdata('success','Berhasil favoritkan pesan');
			if ($menu == 'list') {
				Redirect('Feedback/listPesan');
			}elseif ($menu == 'kirim') {
				Redirect('Feedback/terkirimPesan');
			}elseif ($menu == 'penting') {
				Redirect('Feedback/pentingPesan');
			}elseif ($menu == 'detail') {
				Redirect('Feedback/bacaPesan/'.$ids);
			}
		}elseif ($tipe == "unfavorit") {
			$id 	= ['id' => $ids];
			$data = [
				'favorit'	=> 'false'
			];
			$this->crud->edit($id,$data,'tb_m_pesan');
			$this->session->set_flashdata('success','Berhasil hapus favorit pesan');
			if ($menu == 'list') {
				Redirect('Feedback/listPesan');
			}elseif ($menu == 'kirim') {
				Redirect('Feedback/terkirimPesan');
			}elseif ($menu == 'penting') {
				Redirect('Feedback/pentingPesan');
			}elseif ($menu == 'detail') {
				Redirect('Feedback/bacaPesan/'.$ids);
			}
		}
	}
}