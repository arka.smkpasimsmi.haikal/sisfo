<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil_sekolah extends CI_Controller {

	public function listJurusan()
	{
		$title['title'] = 'Jurusan';
		$data = [
			'jurusan'	=> $this->crud->get('tb_m_jurusan')
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/profil/jurusan',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');

	}

	public function indexStruktur()
	{
		$title['title'] = 'Struktur';
		$data = [
			'struktur'	=> $this->crud->get('tb_m_struktur_organisasi')
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/profil/struktur',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function insertJurusan()
	{
		$this->form_validation->set_rules('nama_jurusan','Nama Jurusan', 'required',
    		['required' => 'Nama Jurusan harus diisi!']);
		$this->form_validation->set_rules('deskripsi','Deskripsi', 'required',
    		['required' => 'Deskripsi harus diisi!']);

		if ($this->form_validation->run()== false) {
			$title['title'] = 'Jurusan';
			$data = [
				'jurusan'	=> $this->crud->get('tb_m_jurusan')
				];

			$this->load->view('templates/server_partial/script_css',$title);
			$this->load->view('templates/server_partial/header');
			$this->load->view('templates/server_partial/sidebar');
			$this->load->view('server/front_end/profil/jurusan',$data);
			$this->load->view('templates/server_partial/footer');
			$this->load->view('templates/server_partial/script_js');
		}else{
			$nama_jurusan	= $this->input->post('nama_jurusan');
			$deskripsi		= $this->input->post('deskripsi');

				$config['upload_path']		= './assets/images/jurusan_images/';
				$config['allowed_types']	= 'jpg|png|jpeg';
				$config['file_name']		= $nama_jurusan.'-'.date('y-m-d');
				$this->load->library('upload', $config);

				if(!$this->upload->do_upload('foto')){
					$this->session->set_flashdata('fail', 'Kesalahan mengunggah gambar!');
    				Redirect('Profil_sekolah/listJurusan');
				}else{
					$foto 	  = $this->upload->data('file_name');
					$data = [
						'nama_jurusan'		=> $nama_jurusan,
						'foto'				=> $foto,
						'deskripsi_jurusan'	=> $deskripsi
					];
					$this->crud->insert($data,'tb_m_jurusan');
					$this->session->set_flashdata('success' , 'Pendaftaran Berhasil!');
					Redirect('Profil_sekolah/listJurusan');
			}
		}

	}

	public function detailJurusan($id)
	{
		$id = ['id' => $id];
		$nama 		= $this->db->get_where('tb_m_jurusan',$id)->row_array();
		$title['title'] = 'Detail Jurusan | '.$nama['nama_jurusan'];
		$data = [
			'jurusan'	=> $this->crud->getById('tb_m_jurusan',$id)
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/profil/jurusan_detail',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function postEditJurusan($ids) {
		$id 			= ['id' => $ids];
		$nama_jurusan	= $this->input->post('nama_jurusan');
		$deskripsi 		= $this->input->post('deskripsi');
		$foto 			= $this->input->post('foto');
		$foto_lama 		= $this->input->post('foto_lama');

		if ($foto !== '') {  
			$config['upload_path']		= './assets/images/jurusan_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['file_name']		= 'struktur_sekolah-'.date('y-m-d');
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('foto')){
				$foto = $foto_lama;
				$hapus = 'false';
    		}else{
    			$upload_data	= $this->upload->data();
    			$foto 	  		= $upload_data['file_name'];
    			$hapus = 'true';
   			}

		}

			$data = [
				'foto'				=> $foto,
				'nama_jurusan'		=> $nama_jurusan,
				'deskripsi_jurusan'	=> $deskripsi
			];
			if ($hapus == 'true') {
				$this->crud->deletePhoto($foto_lama,'jurusan_images');
			}
			$this->crud->edit($id,$data,'tb_m_jurusan');
			$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
			redirect(base_url('Profil_sekolah/detailJurusan/').$ids);	
		
	}

	public function deleteJurusan($id)
	{
		$this->crud->delete($id,'tb_m_jurusan');
		$this->session->set_flashdata('success','Sukses hapus data!');
		Redirect('Profil_sekolah/listJurusan');
	}

	public function postEditStruktur($id)
	{
		$id 		= ['id' => $id];
		$foto_lama 	= $this->input->post('foto_lama');
		$deskripsi 	= $this->input->post('deskripsi');
		$foto 		= $this->input->post('foto');

		if ($foto !== '') {  
			$config['upload_path']		= './assets/images/struktur_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['file_name']		= 'struktur_sekolah-'.date('y-m-d');
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('foto')){
				$foto = $foto_lama;
				$hapus = 'false';
    		}else{
    			$upload_data	= $this->upload->data();
    			$foto 	  		= $upload_data['file_name'];
    			$hapus = 'true';
   			}

		}
			$data = [
				'foto'		=> $foto,
				'deskripsi' => $deskripsi	
			];
			if ($hapus == 'true') {
				$this->crud->deletePhoto($foto_lama,'struktur_images');
			}
			$this->crud->edit($id,$data,'tb_m_struktur_organisasi');
			$this->session->set_flashdata('success','Sukses Update data!');
			Redirect('Profil_sekolah/indexStruktur');
		}
}
