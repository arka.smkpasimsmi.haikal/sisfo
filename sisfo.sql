-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2020 at 05:47 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sisfo`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth`
--

CREATE TABLE `auth` (
  `id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role` varchar(10) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth`
--

INSERT INTO `auth` (`id`, `username`, `password`, `role`, `guru_id`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(6, 'admin@gmail.com', '$2y$10$/hdomDftJkoZK.PhnAGMyuCgqKxAaBtTcNbPKdV5Sad2UOx6mcafO', 'admin', 0, '', '2020-09-12 04:32:39', NULL, '2020-09-12 04:33:07'),
(10, 'herni@gmail.com', '$2y$10$bltoa4ub/j36D0LFuyrHUeiKqOpZ2oX/1W7xAwkgY.zpmK8dfR4dG', 'guru', 22, '', '2020-09-15 08:59:49', NULL, NULL),
(11, 'haikal@gmail.com', '$2y$10$x0tAnsVze35tAWDFyX/A9u4roIu95YPJpxcriYhXXmrCej1yYZUxW', 'guru', 23, 'ADMIN', '2020-09-15 14:08:34', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_beranda`
--

CREATE TABLE `tb_m_beranda` (
  `id` int(11) NOT NULL,
  `ikon` varchar(128) NOT NULL,
  `teks_logo` varchar(128) NOT NULL,
  `foto_banner` varchar(128) NOT NULL,
  `judul_banner` varchar(128) NOT NULL,
  `sub_judul_banner` varchar(255) DEFAULT NULL,
  `nama_sekolah` varchar(128) NOT NULL,
  `foto_kepala_sekolah` varchar(128) NOT NULL,
  `nama_kepala_sekolah` varchar(128) NOT NULL,
  `deskripsi_sambutan` text NOT NULL,
  `deskripsi_visi` text NOT NULL,
  `deskripsi_misi` text NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_beranda`
--

INSERT INTO `tb_m_beranda` (`id`, `ikon`, `teks_logo`, `foto_banner`, `judul_banner`, `sub_judul_banner`, `nama_sekolah`, `foto_kepala_sekolah`, `nama_kepala_sekolah`, `deskripsi_sambutan`, `deskripsi_visi`, `deskripsi_misi`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(1, 'SMK_Pasim_Plus_Sukabumi-20-09-072.png', 'Pasim', 'Pasim_Creative_School-20-09-15.jpg', 'Sekolah Kreatif Berbasis IT', 'Jadilah generasi IT', 'SMK Pasim Plus', 'Pasim_Creative_School-20-09-151.jpg', 'Devi Kunaepi, S.Pd.I.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'ADMIN', '2020-09-04 12:08:09', NULL, '2020-09-15 09:08:15');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_bkk`
--

CREATE TABLE `tb_m_bkk` (
  `id` int(11) NOT NULL,
  `item` varchar(128) NOT NULL,
  `judul` varchar(128) NOT NULL,
  `deskripsi` varchar(500) NOT NULL,
  `kategori` varchar(128) NOT NULL,
  `kota` varchar(128) NOT NULL,
  `alamat` varchar(128) NOT NULL,
  `no_telp` varchar(128) NOT NULL,
  `email_perusahaan` varchar(128) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_bkk`
--

INSERT INTO `tb_m_bkk` (`id`, `item`, `judul`, `deskripsi`, `kategori`, `kota`, `alamat`, `no_telp`, `email_perusahaan`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(1, 'Sekretaris_Bank_BRI-20-09-151.jpg', 'Sekretaris Bank BRI', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nobis, illo, nihil! Id molestias quod sapiente consequuntur neque deserunt sequi amet, ut? Error nemo sit accusamus est atque delectus soluta? Laboriosam.', 'serapan', 'Sukabumi', 'Sukabumi', '(026)-39242', 'bri@gmail.com', 'ADMIN', '2020-09-15 14:37:22', NULL, '2020-09-15 14:54:02'),
(2, 'PT_INTI-20-09-151.jpg', 'PT INTI', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nobis, illo, nihil! Id molestias quod sapiente consequuntur neque deserunt sequi amet, ut? Error nemo sit accusamus est atque delectus soluta? Laboriosam.', 'serapan', 'Bandung Barat', 'Cicaheum', '(026)-35232', 'inti@gmail.com', 'ADMIN', '2020-09-15 14:38:40', NULL, '2020-09-15 14:53:41'),
(3, 'Manager_dtop-20-09-15.png', 'Manager dtop', 'Lorem ipsum dolor sit amet consectetur adipisicing, elit. Laborum dicta, eius optio quod similique aspernatur necessitatibus tempore exercitationem hic assumenda quidem voluptates repellat voluptatum nam praesentium debitis, atque consequuntur dolor?', 'serapan', 'Bogor', 'Lorem ipsum dolor sit amet consectetur adipisicing, elit. Laborum dicta, eius optio quod similique aspernatur necessitatibus tem', '(026)-63234', 'dtop@gmail.com', 'ADMIN', '2020-09-15 15:26:03', NULL, NULL),
(4, '29a281a0087e178bc6c1b97a740ddf0d.jpg', 'Lowongan Community Officer', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Neque quis consequuntur unde quaerat, aspernatur eveniet iste culpa nemo dolorem rerum minima ipsam nobis molestias officiis molestiae adipisci ab, accusamus ipsum. lorem', 'lowongan', '', '', '', '', 'ADMIN', '2020-09-15 15:41:06', NULL, '2020-09-15 15:44:43'),
(5, '797259e12b1deb021b8d64ab4bbeeb52.png', 'Kasir Alfamart', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam sed maxime veniam cum. Laudantium nesciunt odit tempore a eos dolorum. Nam facilis iure quo aperiam dignissimos similique blanditiis adipisci tempora!', 'serapan', 'Bandung Barat', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam sed maxime veniam cum. Laudantium nesciunt odit tempore a eos dol', '(026)-43223', 'alfa@gmail.com', 'ADMIN', '2020-09-15 15:44:26', NULL, NULL),
(6, 'bb10fd2043c12524e40654b789d70cb6.jpg', 'Lowongan Kurir Ninja', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam sed maxime veniam cum. Laudantium nesciunt odit tempore a eos dolorum. Nam facilis iure quo aperiam dignissimos similique blanditiis adipisci tempora!', 'lowongan', '', '', '', '', 'ADMIN', '2020-09-15 15:45:52', NULL, NULL),
(7, 'd0a842dbd25ef01a9344636621bf215c.jpg', 'Lowongan Office Boy', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam sed maxime veniam cum. Laudantium nesciunt odit tempore a eos dolorum. Nam facilis iure quo aperiam dignissimos similique blanditiis adipisci tempora!', 'lowongan', '', '', '', '', 'ADMIN', '2020-09-15 15:46:17', NULL, NULL),
(8, '16a0e1699b44c5d77b36d9374ff24348.jpg', 'Lowongan Karyawan Alfa', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam sed maxime veniam cum. Laudantium nesciunt odit tempore a eos dolorum. Nam facilis iure quo aperiam dignissimos similique blanditiis adipisci tempora!', 'lowongan', '', '', '', '', 'ADMIN', '2020-09-15 15:46:39', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_galeri`
--

CREATE TABLE `tb_m_galeri` (
  `id` int(11) NOT NULL,
  `item` varchar(128) NOT NULL,
  `judul` varchar(128) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `kategori` varchar(128) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_galeri`
--

INSERT INTO `tb_m_galeri` (`id`, `item`, `judul`, `deskripsi`, `kategori`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(9, 'Program_Ayo_Ngaji-20-09-15.jpg', 'Program Ayo Ngaji', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dol', 'foto', 'ADMIN', '2020-09-15 04:14:29', NULL, NULL),
(10, 'Seminar_Robotik-20-09-15.jpg', 'Seminar Robotik', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dol', 'foto', 'ADMIN', '2020-09-15 04:14:56', NULL, NULL),
(11, 'Hardiknas-20-09-15.jpg', 'Hardiknas', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dol', 'foto', 'ADMIN', '2020-09-15 04:15:12', NULL, NULL),
(12, 'Tausiah_Ramadan-20-09-15.jpg', 'Tausiah Ramadan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dol', 'foto', 'ADMIN', '2020-09-15 04:15:31', NULL, NULL),
(13, 'Sanlat_Ramadan-20-09-15.jpg', 'Sanlat Ramadan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dol', 'foto', 'ADMIN', '2020-09-15 04:16:11', NULL, NULL),
(14, 'https://www.youtube.com/watch?v=ptFsJ0FWtm4&list=PL0xE8z9Mp4WMXEqHJGBI2wvcXLRXJ3IjR', 'SMK Pasim Peduli Corona', 'Ditengah masa pandemi covid-19 SMK Pasim Plus hadir dengan program Pasim Peduli dengan memberikan sembako kepada masyarakat yang terkena dampak corona, ini daharapkan bisa meringankan beban ekonomi masyarakat dan turut peduli sesama dalam menyambut Ramada', 'video', 'ADMIN', '2020-09-15 04:18:36', NULL, NULL),
(15, 'https://www.youtube.com/watch?v=zp2wG4XdsRA&list=PL0xE8z9Mp4WMXEqHJGBI2wvcXLRXJ3IjR&index=3', 'Profile SMK Pasim Tahun 2015', 'PROFILE SMK PASIM PLUS 2015', 'video', 'ADMIN', '2020-09-15 04:19:33', NULL, NULL),
(16, 'https://www.youtube.com/watch?v=ttCQYWohatA&list=PL0xE8z9Mp4WMXEqHJGBI2wvcXLRXJ3IjR&index=5', 'Pasim Creative School 2016', 'Pasim Creative School 2016', 'video', 'ADMIN', '2020-09-15 04:20:07', NULL, NULL),
(18, 'https://www.youtube.com/watch?v=MxmcarxRJOg&list=PL0xE8z9Mp4WMXEqHJGBI2wvcXLRXJ3IjR&index=8', 'Teknik Green Screen', 'Tutorial ini akan menunjukkan kepada Anda cara untuk menggunakan green screen /chroma key dengan cara yang kreatif untuk membuat efek unik.', 'video', 'ADMIN', '2020-09-15 04:21:19', NULL, NULL),
(19, 'https://www.youtube.com/watch?v=95cWC-Wtgt8&list=PL0xE8z9Mp4WMXEqHJGBI2wvcXLRXJ3IjR&index=11', 'Ramadan 2020-Sambutan Kepala SMK Pasim Plus', 'Sambutan Kepala SMK Pasim Plus', 'video', 'ADMIN', '2020-09-15 04:21:53', NULL, NULL),
(20, 'https://www.youtube.com/watch?v=JMDR4Avg8eA&list=PL0xE8z9Mp4WMXEqHJGBI2wvcXLRXJ3IjR&index=15', 'OUTBOND SMK PASIM', 'OUTBOND SMK PASIM PALING SERUUUUUU,,,,', 'video', 'ADMIN', '2020-09-15 04:22:25', NULL, NULL),
(21, 'https://www.youtube.com/watch?v=ArQp2HjOPGc&list=PL0xE8z9Mp4WMXEqHJGBI2wvcXLRXJ3IjR&index=16', 'HEROBOT SMK PASIM PLUS SUKABUMI', 'HEROBOT SMK PASIM PLUS SUKABUMI', 'video', 'ADMIN', '2020-09-15 04:22:56', NULL, NULL),
(22, 'https://www.youtube.com/watch?v=cIBKUrpkooo&list=PL0xE8z9Mp4WMXEqHJGBI2wvcXLRXJ3IjR&index=23', 'PPDB Online 2020', 'Pendaftaran melalui website : \r\nppdb.pasimcreativeschool.com\r\nppdb.pasimcreativeschool.com\r\nppdb.pasimcreativeschool.com', 'video', 'ADMIN', '2020-09-15 04:23:39', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_guru`
--

CREATE TABLE `tb_m_guru` (
  `id` int(11) NOT NULL,
  `nama_guru` varchar(128) NOT NULL,
  `mapel` varchar(128) NOT NULL,
  `tgl_masuk` varchar(128) NOT NULL,
  `foto` varchar(128) NOT NULL,
  `facebook` varchar(128) DEFAULT NULL,
  `instagram` varchar(128) DEFAULT NULL,
  `gmail` varchar(128) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_guru`
--

INSERT INTO `tb_m_guru` (`id`, `nama_guru`, `mapel`, `tgl_masuk`, `foto`, `facebook`, `instagram`, `gmail`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(22, 'Herni', 'Agama Islam', '08/14/2018', 'Herni-20-09-15.png', '', 'haikalputrr', '', '', '2020-09-15 08:59:49', NULL, '2020-09-15 13:52:50'),
(23, 'Muhamad Haikal Mulya Putera', 'Matematika', '09/03/2014', 'a5a4be62ef979e56bd832a4a1dc36a3f.jpg', 'https://web.facebook.com/Haikal.ranger72/', 'haikalputrr', '', 'ADMIN', '2020-09-15 14:08:34', NULL, '2020-09-15 14:28:43');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_jurusan`
--

CREATE TABLE `tb_m_jurusan` (
  `id` int(11) NOT NULL,
  `nama_jurusan` varchar(128) NOT NULL,
  `foto` varchar(128) NOT NULL,
  `deskripsi_jurusan` text NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(128) NOT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_by` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_m_jurusan`
--

INSERT INTO `tb_m_jurusan` (`id`, `nama_jurusan`, `foto`, `deskripsi_jurusan`, `created_dt`, `created_by`, `changed_dt`, `changed_by`) VALUES
(12, 'Otomatisasi Tata Kelola Perkantoran', 'struktur_sekolah-20-09-15.jpg', 'Siswa pada kompetensi ini dibimbing untuk menguasai teknik manajemen perkantoran, baik front office maupun back office, dengan prospektif menjadi front officer maupun sekretaris.', '2020-09-15 03:59:00', '', '2020-09-15 03:59:40', NULL),
(13, 'Akuntansi', 'Akuntansi-20-09-15.jpg', 'Kompetensi ini diharapkan siswa menguasai teknik service accounting, trade & manufacrure, industri, koperasi dan perbankan secara manual dan komputerisasi dengan prospektif dapat menjadi profesional accounting.', '2020-09-15 04:00:16', '', NULL, NULL),
(14, 'Broadcasting TV', 'Broadcasting_TV-20-09-15.jpg', 'Produksi dan Siaran Program Televisi (PSPT)\r\nKompetensi yang disampaikan pada program ini diantaranya:\r\nTeknik penulisan naskah\r\nManajemen produski\r\nTata kamera\r\nTata cahaya\r\nPenyutradaraan\r\nTata suara\r\nArtistik\r\nEditing\r\nTeknik siaran radio\r\nDan desain grafis', '2020-09-15 04:00:46', '', NULL, NULL),
(15, 'Film Animasi', 'Film_Animasi-20-09-15.jpg', 'Sebagi salah satu industri kreatif yang diandalkan menjadi sorotan bagi kami untuk membimbing siswa agar dapat menguasai teknik-teknik produksi film animasi baik 2D atau 3D sehingga profesi animator dapat digeluti setelah lulus.', '2020-09-15 04:01:13', '', NULL, NULL),
(16, 'Rekayasa Perangkat Lunak', 'Rekayasa_Perangkat_Lunak-20-09-15.jpg', 'Pengembangan sofware-software dan program aplikasi merupakan kompetensi yang disampaikan pada program studi ini. Dengan prospektif dapat menjadi analisis program, programmer, dan programmer developer.\r\n', '2020-09-15 04:01:45', '', NULL, NULL),
(17, 'Teknik Komputer dan Jaringan', 'Teknik_Komputer_dan_Jaringan-20-09-15.jpg', 'Pada kompetensi ini siswa dibekali teknik komputer dan Instalasi jaringan, dengan prospektif dapat menjadi teknisi komputer, teknisi jaringan dan administrasi jaringan.', '2020-09-15 04:02:44', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_kontak`
--

CREATE TABLE `tb_m_kontak` (
  `id` int(11) NOT NULL,
  `nama_sekolah` varchar(128) NOT NULL,
  `kota` varchar(128) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telepon` varchar(128) NOT NULL,
  `email_sekolah` varchar(128) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_kontak`
--

INSERT INTO `tb_m_kontak` (`id`, `nama_sekolah`, `kota`, `alamat`, `telepon`, `email_sekolah`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(1, 'SMK Pasim Plus', 'Sukabumi', 'Pasim prana', '019280192809182', 'pasim@gmail.com', 'ADMIN', '2020-09-10 04:52:28', NULL, '2020-09-15 09:07:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_navbar`
--

CREATE TABLE `tb_m_navbar` (
  `id` int(11) NOT NULL,
  `nama_navbar` varchar(128) NOT NULL,
  `link` varchar(128) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_pesan`
--

CREATE TABLE `tb_m_pesan` (
  `id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `subject` varchar(128) DEFAULT NULL,
  `deskripsi` varchar(500) DEFAULT NULL,
  `status` varchar(128) DEFAULT NULL,
  `tipe_pesan` varchar(128) NOT NULL,
  `favorit` enum('true','false') NOT NULL,
  `sampah` varchar(128) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_ppdb`
--

CREATE TABLE `tb_m_ppdb` (
  `id` int(11) NOT NULL,
  `nama_siswa` varchar(128) NOT NULL,
  `ijazah` varchar(128) NOT NULL,
  `sku` varchar(128) NOT NULL,
  `akk` varchar(128) NOT NULL,
  `kk` varchar(128) NOT NULL,
  `pernyataan` varchar(255) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_prestasi`
--

CREATE TABLE `tb_m_prestasi` (
  `id` int(11) NOT NULL,
  `foto` varchar(128) NOT NULL,
  `nama_prestasi` varchar(128) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `tanggal_prestasi` varchar(128) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_prestasi`
--

INSERT INTO `tb_m_prestasi` (`id`, `foto`, `nama_prestasi`, `deskripsi`, `tanggal_prestasi`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(12, 'Juara_1_Lomba_Basket-12132018.jpg', 'Juara 1 Lomba Basket', '\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure d', '12/13/2018', '', '2020-09-15 04:36:05', NULL, NULL),
(13, 'Olimpiade_Mikrotik_Nasional_2017-03112020.jpg', 'Olimpiade Mikrotik Nasional 2017', '\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure d', '03/11/2020', '', '2020-09-15 04:36:44', NULL, NULL),
(14, 'Juara_Fotografi-07092017.jpg', 'Juara Fotografi', '\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure d', '07/09/2017', '', '2020-09-15 04:37:09', NULL, NULL),
(15, 'Juara_Nasyid-02062018.jpg', 'Juara Nasyid', '\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure d', '02/06/2018', '', '2020-09-15 04:37:38', NULL, NULL),
(16, 'Juara_Paskibra-10202019.jpg', 'Juara Paskibra', '\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure d', '10/20/2019', '', '2020-09-15 04:38:00', NULL, NULL),
(17, 'Juara_Fashion_Show-06162020.jpg', 'Juara Fashion Show', '\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure d', '06/16/2020', '', '2020-09-15 04:38:23', NULL, NULL),
(18, 'Juara_Scout_Pasim-09122020.jpg', 'Juara Scout Pasim', '\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure d', '09/12/2020', '', '2020-09-15 04:39:18', NULL, NULL),
(19, 'Juara_Basket-09152020.jpg', 'Juara Basket', '\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure d', '09/15/2020', '', '2020-09-15 04:39:32', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_struktur_organisasi`
--

CREATE TABLE `tb_m_struktur_organisasi` (
  `id` int(11) NOT NULL,
  `foto` varchar(128) NOT NULL,
  `deskripsi` varchar(1000) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_struktur_organisasi`
--

INSERT INTO `tb_m_struktur_organisasi` (`id`, `foto`, `deskripsi`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(1, 'struktur_sekolah-20-09-15.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, iste. Nostrum ratione rerum eum, dicta perspiciatis vel, nulla et aliquam qui, distinctio quisquam asperiores id sit natus voluptate labore ea.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, iste. Nostrum ratione rerum eum, dicta perspiciatis vel, nulla et aliquam qui, distinctio quisquam asperiores id sit natus voluptate labore ea.aa', 'ADMIN', '2020-08-25 04:19:23', NULL, '2020-09-15 04:50:43');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_sub_navbar`
--

CREATE TABLE `tb_m_sub_navbar` (
  `id` int(11) NOT NULL,
  `nama_sub_navbar` varchar(128) NOT NULL,
  `link` varchar(128) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_r_navbar`
--

CREATE TABLE `tb_r_navbar` (
  `id` int(11) NOT NULL,
  `id_navbar` int(11) NOT NULL,
  `id_sub_navbar` int(11) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_beranda`
--
ALTER TABLE `tb_m_beranda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_bkk`
--
ALTER TABLE `tb_m_bkk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_galeri`
--
ALTER TABLE `tb_m_galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_guru`
--
ALTER TABLE `tb_m_guru`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_jurusan`
--
ALTER TABLE `tb_m_jurusan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_kontak`
--
ALTER TABLE `tb_m_kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_navbar`
--
ALTER TABLE `tb_m_navbar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_pesan`
--
ALTER TABLE `tb_m_pesan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_ppdb`
--
ALTER TABLE `tb_m_ppdb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_prestasi`
--
ALTER TABLE `tb_m_prestasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_struktur_organisasi`
--
ALTER TABLE `tb_m_struktur_organisasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_sub_navbar`
--
ALTER TABLE `tb_m_sub_navbar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_r_navbar`
--
ALTER TABLE `tb_r_navbar`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth`
--
ALTER TABLE `auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_m_beranda`
--
ALTER TABLE `tb_m_beranda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_m_bkk`
--
ALTER TABLE `tb_m_bkk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_m_galeri`
--
ALTER TABLE `tb_m_galeri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tb_m_guru`
--
ALTER TABLE `tb_m_guru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tb_m_jurusan`
--
ALTER TABLE `tb_m_jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tb_m_kontak`
--
ALTER TABLE `tb_m_kontak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_m_navbar`
--
ALTER TABLE `tb_m_navbar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_m_pesan`
--
ALTER TABLE `tb_m_pesan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tb_m_ppdb`
--
ALTER TABLE `tb_m_ppdb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_m_prestasi`
--
ALTER TABLE `tb_m_prestasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tb_m_struktur_organisasi`
--
ALTER TABLE `tb_m_struktur_organisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_m_sub_navbar`
--
ALTER TABLE `tb_m_sub_navbar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_r_navbar`
--
ALTER TABLE `tb_r_navbar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
